from flask_restx import Api
from flask import Blueprint, abort, Response
import mysql.connector
from .main.controller.task_controller import api as task_controller_ns
from .main.controller.father_task_controller import api as father_task_controller_ns
from .main.controller.metadata_controller import api as metadata_controller_ns
from.main.controller.errors import *


blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='Relay Manage Events',
          version='1.0',
          description='lala'
          )

api.add_namespace(task_controller_ns, path='/task')
api.add_namespace(father_task_controller_ns, path='/father-task')
api.add_namespace(metadata_controller_ns, path='/metadata')


@api.errorhandler(mysql.connector.Error)
def handle_user_does_not_exist_error(error):
    print(error)
    abort(500)


@api.errorhandler(ResourceNotFound)
def handle_resource_not_found(error):
    return {'message': error.message}, error.code


@api.errorhandler(Exception)
def handle_global_exeption(error):
    print(error)
    abort(500)
