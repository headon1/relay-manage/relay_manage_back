from flask_restx import Namespace, fields


class TaskDto:

    api = Namespace(
        'task', description='task endpoints')
    _task = api.model('task', {
        'id': fields.Integer(description='UID'),
        'title': fields.String(required=True, description='title'),
        'description': fields.String(required=True, description='description'),
        'type_id': fields.Integer(required=True),
        'domain_id': fields.Integer(required=True),
        'father_id': fields.Integer(required=True),
        'crew': fields.Raw(),
        'status': fields.String(),
        'start': fields.String(description='start time'),
        'end': fields.String(description='sub mission end time'),
        'allDay': fields.Boolean(description='allDay'),
        'create_time': fields.DateTime(description='creation time'),
        'modify_time': fields.DateTime(description='modify time'),


    })


class FatherTaskDto:
    api = Namespace(
        'father-task', description='father_task endpoints')
    father_task = api.model('father_task', {
        # 'id': fields.String(required=True, description='UID'),
        # 'title': fields.String(required=True, description='sub mission title'),
        # 'description': fields.String(required=True, description='sub mission description'),
        # 'type_id': fields.Integer(required=True),
        # 'domain_id': fields.Integer(required=True),
        # 'father_id': fields.Integer(required=True),
        # 'crew': fields.Raw(),
        # 'status': fields.Integer(),
        # 'start': fields.String(required=True, description='sub mission start time'),
        # 'end': fields.String(required=True, description='sub mission end time'),


    })


class MetadataDto:
    api = Namespace(
        'metadata', description='metadata endpoints')
