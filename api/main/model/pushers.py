import datetime
from .config import get_db_con, QUERIES


def create_row(query_name, params):
    query = QUERIES[query_name]
    con = get_db_con()
    cur = con.cursor()
    cur.execute(query, params)
    con.commit()
    id = cur.lastrowid
    con.close()
    return id
