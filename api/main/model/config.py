import mysql.connector


def get_db_con():
    con = mysql.connector.connect(host='localhost',
                                  user='root',
                                  password='9261953',
                                  database="relay_tasks_manager", auth_plugin='mysql_native_password')
    return con


QUERIES = {
    'GET_ALL_TASKS': """SELECT * from task""",

    'GET_TASK_BY_ID': """SELECT * from task where id =%(id)s""",

    'UPDATE_TASK': """UPDATE task SET title =%(title)s, description = %(description)s, type_id =%(type_id)s, domain_id=%(domain_id)s ,father_id=%(father_id)s, crew =%(crew)s, status=%(status)s, start=%(start)s,end=%(end)s, allDay=%(allDay)s where id =%(id)s""",
    'CREATE_TASK': """INSERT INTO task ( title,description,type_id,domain_id,father_id,crew,start,end, allDay) VALUES(%(title)s, %(description)s, %(type_id)s, %(domain_id)s, %(father_id)s, %(crew)s, %(start)s, %(end)s, %(allDay)s)""",
    'DELETE_TASK': """DELETE FROM TASK WHERE ID = %(id)s""",

    'GET_TASKS_BY_FATHER_ID': """SELECT * from task WHERE father_id =%(id)s order by start""",

    'UPDATE_FATHER_TASK': """UPDATE father_task SET title =%(title)s, description =%(description)s, domain_id=%(domain_id)s , status=%(status)s, urgency=%(urgency)s,start=%(start)s ,end=%(end)s , deadline=%(deadline)s ,added_files=%(added_files)s where id =%(id)s""",
    'CREATE_FATHER_TASK': """INSERT INTO father_task (title, description, domain_id, urgency) VALUES(%(title)s, %(description)s, %(domain_id)s, %(urgency)s )""",

    'GET_FATHER_TASK_BY_ID': """SELECT * from father_task where id =%(id)s""",

    'GET_DOMAINS': """SELECT id,name FROM domain""",
    'GET_TASK_TYPES': """SELECT id,name FROM task_type""",
    'GET_URGENCIES': """SHOW COLUMNS FROM relay_tasks_manager.father_task LIKE 'urgency'""",
    'GET_STATUSES': """SHOW COLUMNS FROM relay_tasks_manager.father_task LIKE 'status'"""
    #  'GET_ALL_TASKS_FROM_FATHER': """SELECT task.id, task.title, task.description, task_type.name as type, domain.name as domain, task.father_id, task.crew, task.status, task.start, task.end FROM task
    # INNER JOIN task_type ON task_type.id = task.type_id
    # INNER JOIN domain ON domain.id = task.domain_id WHERE task.father_id = {} order by task.start""",

}


class QUERIES_NAMES:
    GET_ALL_TASKS = 'GET_ALL_TASKS'
    GET_TASK_BY_ID = 'GET_TASK_BY_ID'
    GET_TASKS_BY_FATHER_ID = 'GET_TASKS_BY_FATHER_ID'
    GET_FATHER_TASK_BY_ID = 'GET_FATHER_TASK_BY_ID'
    GET_DOMAINS = 'GET_DOMAINS'
    GET_TASK_TYPES = 'GET_TASK_TYPES'
    UPDATE_TASK = 'UPDATE_TASK'
    CREATE_TASK = 'CREATE_TASK'
    DELETE_TASK = 'DELETE_TASK'
    GET_URGENCIES = 'GET_URGENCIES'
    GET_STATUSES = 'GET_STATUSES'
    UPDATE_FATHER_TASK = 'UPDATE_FATHER_TASK'
    CREATE_FATHER_TASK = 'CREATE_FATHER_TASK'
