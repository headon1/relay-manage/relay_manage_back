import mysql.connector

cnx = mysql.connector.connect(host='localhost',
                              user='root',
                              password='9261953',
                              database="relay_tasks_manager", auth_plugin='mysql_native_password')

cur = cnx.cursor()
# cur.execute("""CREATE DATABASE RELAY_TASKS_MANAGER""")


# cur.execute("""CREATE TABLE father_task(
#     id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
#     title VARCHAR(30) NOT NULL,
#     description VARCHAR(300) NOT NULL,
#     domain_id INT UNSIGNED,
#     FOREIGN KEY (domain_id) REFERENCES domain(id),
#     status ENUM( 'ממתין',
#     'מתוכנן',
#     'בביצוע',
#     'הושלם' ) DEFAULT 'ממתין',
#     urgency ENUM ('רגיל',
#     'דחוף',
#     'דחוף במיוחד') DEFAULT 'רגיל',
#     start DATETIME,
#     end DATETIME,
#     deadline DATETIME,
#     added_files BLOB,
#     create_time DATETIME DEFAULT CURRENT_TIMESTAMP,
#     modify_time DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
# )""")


# cur.execute("""CREATE TABLE task(
#     id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
#     title VARCHAR(30) NOT NULL,
#     description VARCHAR(300) NOT NULL,
#     type_id INT UNSIGNED,
#     FOREIGN KEY (type_id) REFERENCES task_type(id),
#     domain_id INT UNSIGNED,
#     FOREIGN KEY (domain_id) REFERENCES domain(id),
#     father_id INT UNSIGNED,
#     FOREIGN KEY (father_id) REFERENCES father_task(id),
#     crew VARCHAR(300),
#      status ENUM( 'ממתין',
#     'מתוכנן',
#     'בביצוע',
#     'הושלם' ) DEFAULT 'ממתין',
#     start DATETIME,
#     end DATETIME,
#     allDay BOOLEAN,
#     create_time DATETIME DEFAULT CURRENT_TIMESTAMP,
#     modify_time DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
# )""")


# cur.execute("""CREATE TABLE domain(
#     id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
#     name VARCHAR(30) NOT NULL,
#     create_time DATETIME DEFAULT CURRENT_TIMESTAMP
# )""")


# cur.execute("""INSERT INTO task_type (name)
# VALUES('לחפור הכנה לתשתית')""")

# cur.execute("""INSERT INTO task_type (name)
# VALUES('להניח תשתית צנרת')""")

# cur.execute("""INSERT INTO task_type (name)
# VALUES('להניח תשתית כבלים')""")

# cur.execute("""INSERT INTO task_type (name)
# VALUES('לחוות את הכבלים')""")

# cur.execute("""INSERT INTO task_type (name)
# VALUES('לסדר את האתר למסירה')""")


# cur.execute("""INSERT INTO father_task ( title,description,domain_id) VALUES('תחנת שנאים גני תקווה',
# 'הקמת תחנה חדשה בתקן יורו 2 בהספק 200 מגה ואט'
# ,4)""")

# cur.execute("""INSERT INTO father_task ( title,description,domain_id) VALUES('עבודות תחזוקה אתר ירקון מזרח',
# 'בדיקת עשור - כולל הכפלת שנאי תת קרקעי'
# ,3)""")


cur.execute("""INSERT INTO task ( title,description,type_id,domain_id,father_id,crew) VALUES('אאאאאאאאאאאאאאאאאאאאאאאאאאאאא',
'ציוד דרוש YAMAHAR 40T X 2', 1,1,1,
'["אבי כהן" ,"דן שלום"]'
)""")


cur.execute("""INSERT INTO task ( title,description,type_id,domain_id,father_id,crew) VALUES('הנחת תשתית צנרת',
'ציוד דרוש VOLVO XT500', 2,2,1,
'["רון כהן" ,"יותם ירון"]'
)""")


cur.execute("""INSERT INTO task ( title,description,type_id,domain_id,father_id,crew) VALUES('פריסת כבלים',
'ציוד דרוש MACKITA 443', 3,3,1,
'["שלו דהן" ,"ירון מנשה"]'
)""")


cur.execute("""INSERT INTO task ( title,description,type_id,domain_id,father_id,crew) VALUES('חיוות שנאי',
'לעבוד על פי מפרט קבלן כפי שפורט',
4,4,1,
'["שמחה מישייב"]')""")


cur.execute("""INSERT INTO task ( title,description,type_id,domain_id,father_id,crew) VALUES('סידור האתר לקראת מסירה',
'לשים לב שמפסל לא חורג מ3 מעלות',
5,5,1,'["עוז רוזן", "שלום אברגיל"]')""")

# cur.execute("""INSERT INTO task ( title,description,type_id,domain_id,father_id,crew) VALUES('הוצאת שנאי תת קרקעי ירקון מזרח',
# 'ציוד דרוש YAMAHAR 40T X 2', 1,1,2,'["אבי כהן", "דן שלום"]')""")


# cur.execute("""INSERT INTO task ( title,description,type_id,domain_id,father_id,crew) VALUES('הכפלת שנאי ירקון מזרח',
# 'ציוד דרוש VOLVO XT500', 2,2,2,
# '["ליאור גורן", "רון שלום"]')""")


# cur.execute("""INSERT INTO task ( title,description,type_id,domain_id,father_id,crew) VALUES('פריסת כבלים',
# 'ציוד דרוש MACKITA 443', 3,3,2,'["אור שבתאי"]')""")


# cur.execute("""INSERT INTO task ( title,description,type_id,domain_id,father_id,crew) VALUES('בדיקה כוללת ירקון מזרח',
# 'לעבוד על פי מפרט קבלן כפי שפורט',
# 4,4,2,
# '["שמחה מישייב"]')""")


# cur.execute("""INSERT INTO task ( title,description,type_id,domain_id,father_id,crew) VALUES( 'ציפוי לכלל המתקן ירקון מזרח',
# 'לשים לב שמפסל לא חורג מ3 מעלות',
# 5,5,1,'["עוז רוזן", "שלום אברגיל"]')""")


cnx.commit()
cnx.close()
