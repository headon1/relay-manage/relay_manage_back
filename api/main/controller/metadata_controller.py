import re
from ..util.dto import MetadataDto
from flask_restx import Resource
from flask import request, jsonify
from ..model import QUERIES_NAMES, fetch_multiple_rows, fetch_one_row
import time
import json

api = MetadataDto.api


@ api.route('/domains')
class Domains(Resource):
    def get(self):
        """get domains ids and names"""
        result = fetch_multiple_rows(QUERIES_NAMES.GET_DOMAINS)
        # domains = {item['id']: item['name'] for item in result}

        return result


@ api.route('/task-types')
class Task_Types(Resource):
    @ api.doc('get task types ids names')
    def get(self):
        """get task types ids names"""
        result = fetch_multiple_rows(QUERIES_NAMES.GET_TASK_TYPES)
        # task_types = {item['id']: item['name'] for item in result}

        return result


@ api.route('/urgencies')
class Urgencies(Resource):
    @ api.doc('get urgency options')
    def get(self):
        """get task types ids names"""
        result = fetch_one_row(QUERIES_NAMES.GET_URGENCIES)
        enum = result['Type']
        return Utils.parse_enum(enum)


@api.route('/statuses')
class Statuses(Resource):
    @ api.doc('get status options')
    def get(self):
        """get task types ids names"""
        result = fetch_one_row(QUERIES_NAMES.GET_STATUSES)
        enum = result['Type']
        return Utils.parse_enum(enum)


@ api.route('/domain-staff/<id>')
class DomainStagg(Resource):
    @ api.doc('Domain staff by domain ID')
    # @api.marshal_list_with(_user, envelope='data')
    def get(self, id):
        """Get Domain staff by domain ID"""
        staff = ["אבי כהן", "דן שלום", "רון כהן",
                 "יותם ירון", "שלו דהן", "ירון מנשה", "שמחה מישייב", "עוז רוזן", "שלום אברגיל"]
        return staff


class Utils:
    @staticmethod
    def parse_enum(enum):
        enum_str = enum.decode('utf8')
        regex_search = re.search("'(.*)'", enum_str)
        result_list = (regex_search.group(0).replace("'", ""))
        result_list = result_list.split(',')
        return result_list
