from ..util.dto import TaskDto
from flask_restx import Resource
from flask import request, jsonify
from ..model import QUERIES_NAMES, fetch_multiple_rows, fetch_one_row, create_row
from .errors import ResourceNotFound
import datetime

api = TaskDto.api
_task = TaskDto._task


sub_missions = [
    {'id': '1', 'title': "הנחת צינורות", 'description': "desc1",
     'extendedProps': {'type': "Place Pipes", 'crew': ['@diggers', 'moshe cohen'], 'status':'not_scduled'}},
    {'id': '2', 'title': "חיבור שנאי", 'description': "desc2", 'start': None, 'end': None, 'extendedProps': {
        'type': "Install Transistor", 'crew': ['@electric', 'avi cohen'], 'status':'not_scduled'}},
    {'id': '3', 'title': "ניקיון אתר", 'description': "desc3", 'start': None, 'end': None, 'extendedProps': {
        'type': "Clean", 'crew': ['@Cleaners', 'bob bob'], 'status':'not_scduled'}},
]


@ api.route('/all')
class AllTasks(Resource):
    @ api.doc('all tasks')
    # @api.marshal_list_with(_user, envelope='data')
    def get(self):
        """Get all tasks"""
        tasks = fetch_multiple_rows(QUERIES_NAMES.GET_ALL_TASKS)
        tasks = Utils.dump_tasks(tasks)
        return jsonify(tasks)


@ api.route('/<id>')
class TaskByID(Resource):
    @ api.doc('task by id')
    @api.marshal_list_with(_task)
    def get(self, id):
        """Get task by id"""
        task = fetch_one_row(
            QUERIES_NAMES.GET_TASK_BY_ID, {'id': id})
        return task

    def delete(self, id):
        """Delete task by id"""
        task = fetch_one_row(
            QUERIES_NAMES.GET_TASK_BY_ID, {'id': id})
        if not task:
            raise ResourceNotFound('task', id)
        create_row(QUERIES_NAMES.DELETE_TASK, {'id': id})
        return 200


@ api.route('')
class Task(Resource):
    @api.doc(body=_task)
    def put(self):
        """Update task"""
        updated_task = request.json
        updated_task = Utils.parse_task(updated_task)
        task = fetch_one_row(
            QUERIES_NAMES.GET_TASK_BY_ID, updated_task)

        if not task:
            raise ResourceNotFound('task', updated_task['id'])
        create_row(QUERIES_NAMES.UPDATE_TASK, updated_task)
        created_task = fetch_one_row(
            QUERIES_NAMES.GET_TASK_BY_ID, updated_task)
        created_task = Utils.dump_task(created_task)
        return jsonify(created_task)

    @ api.doc(body=_task)
    @api.marshal_with(_task)
    def post(self):
        """Create new task"""
        task = request.json
        task = Utils.parse_task(task)
        id = create_row(QUERIES_NAMES.CREATE_TASK, task)
        created_task = fetch_one_row(
            QUERIES_NAMES.GET_TASK_BY_ID, {'id': id})
        created_task = Utils.dump_task(created_task)
        return created_task


class Utils:
    @staticmethod
    def parse_task(task):
        if task['start']:
            task['start'] = (datetime.datetime.fromisoformat(
                task['start'][:-1]) + datetime.timedelta(hours=3))
        if task['end']:
            task['end'] = (datetime.datetime.fromisoformat(
                task['end'][:-1]) + datetime.timedelta(hours=3))
        if task['start'] and task['end']:
            if task['start'].hour == 0 and task['start'].minute == 0 and task['end'].hour == 0 and task['end'].minute == 0:
                task['allDay'] = True
            else:
                task['allDay'] = False
        else:
            task['allDay'] = False
        task['crew'] = str(task['crew'])
        return task

    @staticmethod
    def parse_tasks(tasks):
        for task in tasks:
            task = Utils.parse_task(task)
        return tasks

    @staticmethod
    def dump_task(task):
        if task['start']:
            task['start'] = task['start'].isoformat()
        if task['end']:
            task['end'] = task['end'].isoformat()
        if task['allDay'] == 0:
            task['allDay'] = False
        else:
            task['allDay'] = True
        task['crew'] = eval(task['crew'])
        return task

    @staticmethod
    def dump_tasks(tasks):
        for task in tasks:
            task = Utils.dump_task(task)
        return tasks
