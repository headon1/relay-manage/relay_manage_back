from ..util.dto import FatherTaskDto
from flask_restx import Resource
from flask import request, jsonify
from ..model import QUERIES_NAMES, fetch_multiple_rows, fetch_one_row, create_row
import time
import datetime
from .errors import ResourceNotFound
from .task_controller import Utils as taskUtils
api = FatherTaskDto.api
# _user = SubmissionDto.


@ api.route('/with-childs/<id>')
class FatherTaskWtihChilds(Resource):
    @ api.doc('Get father father_task with all it father_task childs')
    # @api.marshal_list_with(_user, envelope='data')
    def get(self, id):
        """Get father father_task with all it father_task childs"""
        father_task = fetch_one_row(
            QUERIES_NAMES.GET_FATHER_TASK_BY_ID, {'id': id})
        father_task = Utils.dump_father_task(father_task)
        tasks = fetch_multiple_rows(
            QUERIES_NAMES.GET_TASKS_BY_FATHER_ID, {'id': id})
        tasks = taskUtils.dump_tasks(tasks)
        result = {'father_task': father_task, 'tasks': tasks}
        time.sleep(1)
        return jsonify(result)


@ api.route('/all')
class AllFatherTasks(Resource):
    def get(self, id):
        """Get all father Tasks"""
        pass


@ api.route('/<id>')
class FatherTaskByID(Resource):
    def get(self, id):
        """Get father father_task by id"""
        pass

    def delete(self, id):
        """Delete father father_task by id"""
        pass


@ api.route('')
class FatherTask(Resource):
    def put(self):
        """Update father father_task"""
        """Update father_task"""
        updated_father_task = request.json
        updated_father_task = Utils.parse_father_task(updated_father_task)
        # father_task = fetch_one_row(
        #     QUERIES_NAMES.GET_FATHER_TASK_BY_ID, [updated_father_task['id']])
        # elegant way to get only id out of dict?
        father_task = fetch_one_row(
            QUERIES_NAMES.GET_FATHER_TASK_BY_ID, updated_father_task)

        if not father_task:
            raise ResourceNotFound('father father_task',
                                   updated_father_task['id'])
        # create_row(QUERIES_NAMES.UPDATE_FATHER_TASK, [updated_father_task['title'], updated_father_task['description'],
        #                                               updated_father_task['domain_id'], updated_father_task['status'], updated_father_task['urgency'],  updated_father_task['start'], updated_father_task['end'], updated_father_task['deadline'], updated_father_task['added_files'], updated_father_task['id']])
        create_row(QUERIES_NAMES.UPDATE_FATHER_TASK, updated_father_task)
        created_father_task = fetch_one_row(
            QUERIES_NAMES.GET_FATHER_TASK_BY_ID, {'id': updated_father_task['id']})
        created_father_task = Utils.dump_father_task(created_father_task)
        return jsonify(created_father_task)

    def post(self):
        """Creates new father task"""
        father_task = request.json
        id = create_row(QUERIES_NAMES.CREATE_FATHER_TASK, father_task)
        created_father_task = fetch_one_row(
            QUERIES_NAMES.GET_FATHER_TASK_BY_ID, {'id': id})
        created_father_task = Utils.dump_father_task(created_father_task)
        return jsonify(created_father_task)


class Utils:
    @staticmethod
    def parse_father_task(father_task):
        # if father_task['start'] is None:
        #     father_task['start'] = "NULL"
        # else:
        #     father_task['start'] = "'{}'".format((datetime.datetime.fromisoformat(
        #         father_task['start'][:-1])).strftime('%Y-%m-%d %H:%M:%S'))
        # if father_task['end'] is None:
        #     father_task['end'] = "NULL"
        # else:
        #     father_task['end'] = (datetime.datetime.fromisoformat(
        #         father_task['end'][:-1])).strftime('%Y-%m-%d %H:%M:%S')
        # if father_task['deadline'] is None:
        #     father_task['deadline'] = "NULL"
        # else:
        #     father_task['deadline'] = datetime.datetime(
        #         father_task['deadline'])
        return father_task

    @ staticmethod
    def dump_father_task(father_task):
        if father_task['start']:
            father_task['start'] = father_task['start'].isoformat()
        if father_task['end']:
            father_task['end'] = father_task['end'].isoformat()
        if father_task['deadline']:
            father_task['deadline'] = father_task['deadline'].strftime(
                "%y-%m-%d")
        return father_task
