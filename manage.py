import os
import unittest

from flask_script import Manager
from api import blueprint
from api.main import create_app

app = create_app('dev')
app.register_blueprint(blueprint)

# manager = Manager(app)


# @manager.command
# def run():
#     app.run()


if __name__ == '__main__':
    app.run()
